import React from 'react'


const ResetPasswordView = (props) => (
    <div>
         <form className="form-inline container justify-content-center">
      <div className="form-group mb-2">
        <input style={{ width: "400px", marginRight: "10px" }} type="text" readonly className="form-control" placeholder="email@example.com"/>
      </div>
          <button type="submit" className="btn btn-primary mb-2">Reset Password</button>
        </form>
        <p className="zaten-uye">Üye giriş için <a href="#" onClick={ e => {
            e.preventDefault();
            props.onViewChange(1);
        }

        } >tıklayınız</a></p>
    </div>
)

export default ResetPasswordView;