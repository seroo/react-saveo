import React, { Component } from 'react';
import LoginView from './LoginView';
import SignUpView from './SignUpView';
import PasswordReset from './PasswordReset';



class AuthView extends Component {
    
    // 1: Giris ekrani
    // 2: kayit ekrani
    // 3: sifre reset ekrani

    constructor() {
        super();
        this.state = {
            currentView : 1,

        }
    }

    changeView(newView){
       // console.log("calisti " + newView);
        
        this.setState({
            currentView: newView
        })
        }

    render() {

        const view = this.state.currentView === 1
                        ? <LoginView onViewChange={this.changeView.bind(this)} />
                        : this.state.currentView === 2
                        ? <SignUpView onViewChange={this.changeView.bind(this)} />
                        : <PasswordReset onViewChange={this.changeView.bind(this)} />;
                        

        return (
            <div>
                { view }
            </div>
        )
    }
}

export default AuthView;