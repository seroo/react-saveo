import React from 'react'


const LoginView = (props) => (
    <div>
        <form className="form-inline container justify-content-center">
      <div className="form-group mb-2">
        <input type="text" readonly className="form-control" placeholder="email@example.com"/>
      </div>
      <div className="form-group mx-sm-3 mb-2">
        <input type="password" className="form-control" id="inputPassword2" placeholder="Password"/>
      </div>
          <button type="submit" className="btn btn-primary mb-2">Giris</button>
         
        </form>
        <p className="container" style={{textAlign:"center"}} >
         <a href="#" onClick={ e => {
             e.preventDefault();
             props.onViewChange(3);
         }
             
         } >Sifremi unuttum</a></p>
        <p className="zaten-uye" >Üye olmak için <a href="#" onClick={ e => {
             e.preventDefault();
             props.onViewChange(2);
        }} >tıklayınız</a></p>
    </div>
)

export default LoginView;